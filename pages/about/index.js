import React from "react";
import Page from "../../layout/main";
import Header from "../../components/header.js";
const About = ({ children }) => (
	<Page>
		<Header />
		<div className="breadcrumbs">
			<div className="container">
				<div className="row">
					<div className="col-12">
						<div className="bread-inner">
							<ul className="bread-list">
								<li>
									<a href="main">
										Inicio<i className="ti-arrow-right"></i>
									</a>
								</li>
								<li className="active">
									<a href="about">Nosotros</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section className="about-us section">
			<div className="container">
				<div className="row">
					<div className="col-lg-6 col-12">
						<div className="about-content">
							<h3>
								Welcome To <span>Eshop</span>
							</h3>
							<p>
								Excepteur sint occaecat cupidatat non proident, sunt in culpa
								qui officia deserunt mollit anim id est laborum. sed ut
								perspiciatis unde sunt in culpa qui officia deserunt mollit anim
								id est laborum. sed ut perspiciatis unde omnis iste natus error
								sit voluptatem Excepteu{" "}
							</p>
							<p>
								sunt in culpa qui officia deserunt mollit anim id est laborum.
								sed ut perspiciatis Excepteur sint occaecat cupidatat non
								proident, sunt in culpa qui officia deserunt mollit anim id est
								laborum. sed ut perspi deserunt mollit anim id est laborum. sed
								ut perspi.
							</p>
							<div className="button">
								<a href="blog.html" className="btn">
									Our Blog
								</a>
								<a href="contact.html" className="btn primary">
									Contact Us
								</a>
							</div>
						</div>
					</div>
					<div className="col-lg-6 col-12">
						<div className="about-img overlay">
							<div className="button">
								<a
									href="https://www.youtube.com/watch?v=nh2aYrGMrIE"
									className="video video-popup mfp-iframe"
								>
									<i className="fa fa-play"></i>
								</a>
							</div>
							<img src="assets/images/about.jpg" alt="#" />
						</div>
					</div>
				</div>
			</div>
		</section>
	</Page>
);

export default About;
