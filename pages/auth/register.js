import React from "react";
import Page from "../../layout/main";
import Header from "../../components/header.js";
const Register = ({ children }) => (
	<Page>
		<Header />
		<div className="breadcrumbs">
			<div className="container">
				<div className="row">
					<div className="col-12">
						<div className="bread-inner">
							<ul className="bread-list">
								<li>
									<a href="index1.html">
										Inicio<i className="ti-arrow-right"></i>
									</a>
								</li>
								<li className="active">
									<a href="blog-single.html">Registro</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section className="shop login section">
			<div className="container">
				<div className="row">
					<div className="col-lg-6 offset-lg-3 col-12">
						<div className="login-form">
							<h2>Registro</h2>
							<p>Regístrese para pagar más rápido</p>
							<form className="form" method="post" action="#">
								<div className="row">
									<div className="col-12">
										<div className="form-group">
											<label>
												Tu Nombre<span>*</span>
											</label>
											<input
												type="text"
												name="name"
												placeholder=""
												required="required"
											/>
										</div>
									</div>
									<div className="col-12">
										<div className="form-group">
											<label>
												Tu Email<span>*</span>
											</label>
											<input
												type="text"
												name="email"
												placeholder=""
												required="required"
											/>
										</div>
									</div>
									<div className="col-12">
										<div className="form-group">
											<label>
												Tu Contraseña<span>*</span>
											</label>
											<input
												type="password"
												name="password"
												placeholder=""
												required="required"
											/>
										</div>
									</div>
									<div className="col-12">
										<div className="form-group">
											<label>
												Confirmar Contraseña<span>*</span>
											</label>
											<input
												type="password"
												name="password"
												placeholder=""
												required="required"
											/>
										</div>
									</div>
									<div className="col-12">
										<div className="form-group login-btn">
											<button className="btn" type="submit">
												Registro
											</button>
											<a href="login" className="btn">
												Iniciar Sesión
											</a>
										</div>
										<div className="checkbox">
											<label className="checkbox-inline" for="2">
												<input name="news" id="2" type="checkbox" />
												Suscríbase al boletín
											</label>
										</div>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	</Page>
);

export default Register;
