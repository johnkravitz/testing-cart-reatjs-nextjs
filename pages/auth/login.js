import React from "react";
import Page from "../../layout/main";
import Header from "../../components/header.js";
const Login = ({ children }) => (
	<Page>
		<Header />
		<div className="breadcrumbs">
			<div className="container">
				<div className="row">
					<div className="col-12">
						<div className="bread-inner">
							<ul className="bread-list">
								<li>
									<a href="index1.html">
										Inicio<i className="ti-arrow-right"></i>
									</a>
								</li>
								<li className="active">
									<a href="blog-single.html">Iniciar Sesión</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<section className="shop login section">
			<div className="container">
				<div className="row">
					<div className="col-lg-6 offset-lg-3 col-12">
						<div className="login-form">
							<h2>Iniciar Sesión</h2>
							<p>Regístrese para pagar más rápido</p>
							<form className="form" method="post" action="#">
								<div className="row">
									<div className="col-12">
										<div className="form-group">
											<label>
												Tu Correo<span>*</span>
											</label>
											<input
												type="email"
												name="email"
												placeholder=""
												required="required"
											/>
										</div>
									</div>
									<div className="col-12">
										<div className="form-group">
											<label>
												Tu Contraseña<span>*</span>
											</label>
											<input
												type="password"
												name="password"
												placeholder=""
												required="required"
											/>
										</div>
									</div>
									<div className="col-12">
										<div className="form-group login-btn">
											<button className="btn" type="submit">
												Iniciar Sesión
											</button>
											<a href="register" className="btn">
												Registro
											</a>
										</div>
										<div className="checkbox">
											<label className="checkbox-inline" for="2">
												<input name="news" id="2" type="checkbox" />
												Recordarme
											</label>
										</div>
										<a href="#" className="lost-pass">
											¿Perdiste tu contraseña?
										</a>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</section>
	</Page>
);

export default Login;
