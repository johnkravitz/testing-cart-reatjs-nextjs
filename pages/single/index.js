import React from "react";
import Page from "../../layout/main";
import Header from "../../components/header";
import Details from "./components/Details";
const Single = () => (
	<Page>
		<Header />
		<Details />
	</Page>
);
export default Single;
