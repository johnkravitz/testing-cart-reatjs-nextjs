import React from "react";

const List = () => (
	<div>
		<div className="breadcrumbs">
			<div className="container">
				<div className="row">
					<div className="col-12">
						<div className="bread-inner">
							<ul className="bread-list">
								<li>
									<a href="index1.html">
										Home<i className="ti-arrow-right"></i>
									</a>
								</li>
								<li className="active">
									<a href="blog-single.html">Cart</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div className="shopping-cart section">
			<div className="container">
				<div className="row">
					<div className="col-12">
						<table className="table shopping-summery">
							<thead>
								<tr className="main-hading">
									<th>PRODUCT</th>
									<th>NAME</th>
									<th className="text-center">UNIT PRICE</th>
									<th className="text-center">QUANTITY</th>
									<th className="text-center">TOTAL</th>
									<th className="text-center">
										<i className="ti-trash remove-icon"></i>
									</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td className="image" data-title="No">
										<img src="https://via.placeholder.com/100x100" alt="#" />
									</td>
									<td className="product-des" data-title="Description">
										<p className="product-name">
											<a
												href="#exampleModal"
												data-toggle="modal"
												data-target="#exampleModal"
											>
												Women Dress
											</a>
										</p>
										<p className="product-des">
											Maboriosam in a tonto nesciung eget distingy magndapibus.
										</p>
									</td>
									<td className="price" data-title="Price">
										<span>$110.00 </span>
									</td>
									<td className="qty" data-title="Qty">
										<div className="input-group">
											<div className="button minus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													disabled="disabled"
													data-type="minus"
													data-field="quant[1]"
												>
													<i className="ti-minus"></i>
												</button>
											</div>
											<input
												type="text"
												name="quant[1]"
												className="input-number"
												data-min="1"
												data-max="100"
												value="1"
											/>
											<div className="button plus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													data-type="plus"
													data-field="quant[1]"
												>
													<i className="ti-plus"></i>
												</button>
											</div>
										</div>
									</td>
									<td className="total-amount" data-title="Total">
										<span>$220.88</span>
									</td>
									<td className="action" data-title="Remove">
										<a href="#">
											<i className="ti-trash remove-icon"></i>
										</a>
									</td>
								</tr>
								<tr>
									<td className="image" data-title="No">
										<img src="https://via.placeholder.com/100x100" alt="#" />
									</td>
									<td className="product-des" data-title="Description">
										<p className="product-name">
											<a href="#">Women Dress</a>
										</p>
										<p className="product-des">
											Maboriosam in a tonto nesciung eget distingy magndapibus.
										</p>
									</td>
									<td className="price" data-title="Price">
										<span>$110.00 </span>
									</td>
									<td className="qty" data-title="Qty">
										<div className="input-group">
											<div className="button minus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													disabled="disabled"
													data-type="minus"
													data-field="quant[2]"
												>
													<i className="ti-minus"></i>
												</button>
											</div>
											<input
												type="text"
												name="quant[2]"
												className="input-number"
												data-min="1"
												data-max="100"
												value="2"
											/>
											<div className="button plus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													data-type="plus"
													data-field="quant[2]"
												>
													<i className="ti-plus"></i>
												</button>
											</div>
										</div>
									</td>
									<td className="total-amount" data-title="Total">
										<span>$220.88</span>
									</td>
									<td className="action" data-title="Remove">
										<a href="#">
											<i className="ti-trash remove-icon"></i>
										</a>
									</td>
								</tr>
								<tr>
									<td className="image" data-title="No">
										<img src="https://via.placeholder.com/100x100" alt="#" />
									</td>
									<td className="product-des" data-title="Description">
										<p className="product-name">
											<a href="#">Women Dress</a>
										</p>
										<p className="product-des">
											Maboriosam in a tonto nesciung eget distingy magndapibus.
										</p>
									</td>
									<td className="price" data-title="Price">
										<span>$110.00 </span>
									</td>
									<td className="qty" data-title="Qty">
										<div className="input-group">
											<div className="button minus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													disabled="disabled"
													data-type="minus"
													data-field="quant[3]"
												>
													<i className="ti-minus"></i>
												</button>
											</div>
											<input
												type="text"
												name="quant[3]"
												className="input-number"
												data-min="1"
												data-max="100"
												value="3"
											/>
											<div className="button plus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													data-type="plus"
													data-field="quant[3]"
												>
													<i className="ti-plus"></i>
												</button>
											</div>
										</div>
									</td>
									<td className="total-amount" data-title="Total">
										<span>$220.88</span>
									</td>
									<td className="action" data-title="Remove">
										<a href="#">
											<i className="ti-trash remove-icon"></i>
										</a>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div className="row">
					<div className="col-12">
						<div className="total-amount">
							<div className="row">
								<div className="col-lg-8 col-md-5 col-12">
									<div className="left">
										<div className="coupon">
											<form action="#" target="_blank">
												<input name="Coupon" placeholder="Enter Your Coupon" />
												<button className="btn">Apply</button>
											</form>
										</div>
										<div className="checkbox">
											<label className="checkbox-inline" for="2">
												<input name="news" id="2" type="checkbox" /> Shipping
												(+10$)
											</label>
										</div>
									</div>
								</div>
								<div className="col-lg-4 col-md-7 col-12">
									<div className="right">
										<ul>
											<li>
												Cart Subtotal<span>$330.00</span>
											</li>
											<li>
												Shipping<span>Free</span>
											</li>
											<li>
												You Save<span>$20.00</span>
											</li>
											<li className="last">
												You Pay<span>$310.00</span>
											</li>
										</ul>
										<div className="button5">
											<a href="checkout" className="btn">
												Checkout
											</a>
											<a href="shop" className="btn">
												Continue shopping
											</a>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div className="modal fade" id="exampleModal" role="dialog">
			<div className="modal-dialog" role="document">
				<div className="modal-content">
					<div className="modal-header">
						<button
							type="button"
							className="close"
							data-dismiss="modal"
							aria-label="Close"
						>
							<span className="ti-close" aria-hidden="true"></span>
						</button>
					</div>
					<div className="modal-body">
						<div className="row no-gutters">
							<div className="col-lg-6 col-md-12 col-sm-12 col-xs-12">
								<div className="product-gallery">
									<div className="quickview-slider-active">
										<div className="single-slider">
											<img src="assets/images/bx-slider1.jpg" alt="#" />
										</div>
										<div className="single-slider">
											<img src="assets/images/bx-slider2.jpg" alt="#" />
										</div>
										<div className="single-slider">
											<img src="assets/images/bx-slider3.jpg" alt="#" />
										</div>
										<div className="single-slider">
											<img src="assets/images/bx-slider4.jpg" alt="#" />
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-6 col-md-12 col-sm-12 col-xs-12">
								<div className="quickview-content">
									<h2>Flared Shift Dress</h2>
									<div className="quickview-ratting-review">
										<div className="quickview-ratting-wrap">
											<div className="quickview-ratting">
												<i className="yellow fa fa-star"></i>
												<i className="yellow fa fa-star"></i>
												<i className="yellow fa fa-star"></i>
												<i className="yellow fa fa-star"></i>
												<i className="fa fa-star"></i>
											</div>
											<a href="#"> (1 customer review)</a>
										</div>
										<div className="quickview-stock">
											<span>
												<i className="fa fa-check-circle-o"></i> in stock
											</span>
										</div>
									</div>
									<h3>$29.00</h3>
									<div className="quickview-peragraph">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
											Mollitia iste laborum ad impedit pariatur esse optio
											tempora sint ullam autem deleniti nam in quos qui nemo
											ipsum numquam.
										</p>
									</div>
									<div className="size">
										<div className="row">
											<div className="col-lg-6 col-12">
												<h5 className="title">Size</h5>
												<select>
													<option selected="selected">s</option>
													<option>m</option>
													<option>l</option>
													<option>xl</option>
												</select>
											</div>
											<div className="col-lg-6 col-12">
												<h5 className="title">Color</h5>
												<select>
													<option selected="selected">orange</option>
													<option>purple</option>
													<option>black</option>
													<option>pink</option>
												</select>
											</div>
										</div>
									</div>
									<div className="quantity">
										<div className="input-group">
											<div className="button minus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													disabled="disabled"
													data-type="minus"
													data-field="quant[1]"
												>
													<i className="ti-minus"></i>
												</button>
											</div>
											<input
												type="text"
												name="quant[1]"
												className="input-number"
												data-min="1"
												data-max="1000"
												value="1"
											/>
											<div className="button plus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													data-type="plus"
													data-field="quant[1]"
												>
													<i className="ti-plus"></i>
												</button>
											</div>
										</div>
									</div>
									<div className="add-to-cart">
										<a href="#" className="btn">
											Add to cart
										</a>
										<a href="#" className="btn min">
											<i className="ti-heart"></i>
										</a>
										<a href="#" className="btn min">
											<i className="fa fa-compress"></i>
										</a>
									</div>
									<div className="default-social">
										<h4 className="share-now">Share:</h4>
										<ul>
											<li>
												<a className="facebook" href="#">
													<i className="fa fa-facebook"></i>
												</a>
											</li>
											<li>
												<a className="twitter" href="#">
													<i className="fa fa-twitter"></i>
												</a>
											</li>
											<li>
												<a className="youtube" href="#">
													<i className="fa fa-pinterest-p"></i>
												</a>
											</li>
											<li>
												<a className="dribbble" href="#">
													<i className="fa fa-google-plus"></i>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
);

export default List;
