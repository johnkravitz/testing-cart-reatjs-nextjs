import React from "react";
import Page from "../../layout/main";
import Header from "../../components/header.js";
import CartList from "./components/List.js";
import ShopServices from "../../components/ShopServices.js";
import Newsletters from "../../components/Newsletters.js";
const Cart = () => (
	<Page>
		<Header />
		<CartList />
		<ShopServices />
		<Newsletters />
	</Page>
);
export default Cart;
