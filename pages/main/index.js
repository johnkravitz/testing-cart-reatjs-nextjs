import React from "react";
import Page from "../../layout/main";
import Header from "./components/Header.js";
import HeroSlider from "./components/HeroSlider.js";
import MostPopular from "./components/MostPopular.js";
import ProductArea from "./components/ProductArea.js";
import SmallBanners from "./components/SmallBanners.js";
import ShopServices from "../../components/ShopServices.js";
const Main = ({ children }) => (
	<Page>
		<Header />
		<HeroSlider />
		<SmallBanners />
		<ProductArea />
		<MostPopular />
		<ShopServices />
	</Page>
);
export default Main;
