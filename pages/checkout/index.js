import React from "react";
import Page from "../../layout/main";
import Header from "../../components/header.js";
import CheckoutList from "./components/List.js";
import ShopServices from "../../components/ShopServices.js";
import Newsletters from "../../components/Newsletters.js";
const Checkout = () => (
	<Page>
		<Header />
		<CheckoutList />
		<ShopServices />
		<Newsletters />
	</Page>
);
export default Checkout;
