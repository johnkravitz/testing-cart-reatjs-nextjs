import React from "react";

const GridProducts = () => (
	<div>
		<div class="breadcrumbs">
			<div class="container">
				<div class="row">
					<div class="col-12">
						<div class="bread-inner">
							<ul class="bread-list">
								<li>
									<a href="main">
										Inicio<i class="ti-arrow-right"></i>
									</a>
								</li>
								<li class="active">
									<a href="shop">Productos</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>

		<section className="product-area shop-sidebar shop section">
			<div className="container">
				<div className="row">
					<div className="col-lg-3 col-md-4 col-12">
						<div className="shop-sidebar">
							<div className="single-widget category">
								<h3 className="title">Categorias</h3>
								<ul className="categor-list">
									<li>
										<a href="#">T-shirts</a>
									</li>
									<li>
										<a href="#">jacket</a>
									</li>
									<li>
										<a href="#">jeans</a>
									</li>
									<li>
										<a href="#">sweatshirts</a>
									</li>
									<li>
										<a href="#">trousers</a>
									</li>
									<li>
										<a href="#">kitwears</a>
									</li>
									<li>
										<a href="#">accessories</a>
									</li>
								</ul>
							</div>

							<div className="single-widget range">
								<h3 className="title">Comprar por precio</h3>
								<div className="price-filter">
									<div className="price-filter-inner">
										<div id="slider-range"></div>
										<div className="price_slider_amount">
											<div className="label-input">
												<span>Rango:</span>
												<input
													type="text"
													id="amount"
													name="price"
													placeholder="Agregue su precio"
												/>
											</div>
										</div>
									</div>
								</div>
								<ul className="check-box-list">
									<li>
										<label className="checkbox-inline" for="1">
											<input name="news" id="1" type="checkbox" />
											$20 - $50<span className="count">(3)</span>
										</label>
									</li>
									<li>
										<label className="checkbox-inline" for="2">
											<input name="news" id="2" type="checkbox" />
											$50 - $100<span className="count">(5)</span>
										</label>
									</li>
									<li>
										<label className="checkbox-inline" for="3">
											<input name="news" id="3" type="checkbox" />
											$100 - $250<span className="count">(8)</span>
										</label>
									</li>
								</ul>
							</div>

							<div className="single-widget recent-post">
								<h3 className="title">Publicación Reciente</h3>

								<div className="single-post first">
									<div className="image">
										<img src="https://via.placeholder.com/75x75" alt="#" />
									</div>
									<div className="content">
										<h5>
											<a href="#">Girls Dress</a>
										</h5>
										<p className="price">$99.50</p>
										<ul className="reviews">
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li>
												<i className="ti-star"></i>
											</li>
											<li>
												<i className="ti-star"></i>
											</li>
										</ul>
									</div>
								</div>

								<div className="single-post first">
									<div className="image">
										<img src="https://via.placeholder.com/75x75" alt="#" />
									</div>
									<div className="content">
										<h5>
											<a href="#">Women Clothings</a>
										</h5>
										<p className="price">$99.50</p>
										<ul className="reviews">
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li>
												<i className="ti-star"></i>
											</li>
										</ul>
									</div>
								</div>

								<div className="single-post first">
									<div className="image">
										<img src="https://via.placeholder.com/75x75" alt="#" />
									</div>
									<div className="content">
										<h5>
											<a href="#">Man Tshirt</a>
										</h5>
										<p className="price">$99.50</p>
										<ul className="reviews">
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
											<li className="yellow">
												<i className="ti-star"></i>
											</li>
										</ul>
									</div>
								</div>
							</div>

							<div className="single-widget category">
								<h3 className="title">Marcas</h3>
								<ul className="categor-list">
									<li>
										<a href="#">Forever</a>
									</li>
									<li>
										<a href="#">giordano</a>
									</li>
									<li>
										<a href="#">abercrombie</a>
									</li>
									<li>
										<a href="#">ecko united</a>
									</li>
									<li>
										<a href="#">zara</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
					<div className="col-lg-9 col-md-8 col-12">
						<div className="row">
							<div className="col-12">
								<div className="shop-top">
									<div className="shop-shorter">
										<div className="single-shorter">
											<label>Mostrar:</label>
											<select>
												<option selected="selected">09</option>
												<option>15</option>
												<option>25</option>
												<option>30</option>
											</select>
										</div>
										<div className="single-shorter">
											<label>Ordenar por :</label>
											<select>
												<option selected="selected">Nombre</option>
												<option>Precio</option>
												<option>Talla</option>
											</select>
										</div>
									</div>
									<ul className="view-mode">
										<li className="active">
											<a href="shop-grid.html">
												<i className="fa fa-th-large"></i>
											</a>
										</li>
										<li>
											<a href="shop-list.html">
												<i className="fa fa-th-list"></i>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div className="row">
							<div className="col-lg-4 col-md-6 col-12">
								<div className="single-product">
									<div className="product-img">
										<a href="single">
											<img
												className="default-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<img
												className="hover-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
										</a>
										<div className="button-head">
											<div className="product-action">
												<a
													data-toggle="modal"
													data-target="#exampleModal"
													title="Quick View"
													href="#"
												>
													<i className=" ti-eye"></i>
													<span>Compra rápida</span>
												</a>
												<a title="Wishlist" href="#">
													<i className=" ti-heart "></i>
													<span>Añadir a la lista de deseos</span>
												</a>
												<a title="Compare" href="#">
													<i className="ti-bar-chart-alt"></i>
													<span>Añadir a comparar</span>
												</a>
											</div>
											<div className="product-action-2">
												<a title="Añadir al carrito" href="#">
													Añadir al carrito
												</a>
											</div>
										</div>
									</div>
									<div className="product-content">
										<h3>
											<a href="product-details.html">Women Hot Collection</a>
										</h3>
										<div className="product-price">
											<span>$29.00</span>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 col-12">
								<div className="single-product">
									<div className="product-img">
										<a href="product-details.html">
											<img
												className="default-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<img
												className="hover-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
										</a>
										<div className="button-head">
											<div className="product-action">
												<a
													data-toggle="modal"
													data-target="#exampleModal"
													title="Quick View"
													href="#"
												>
													<i className=" ti-eye"></i>
													<span>Compra rápida</span>
												</a>
												<a title="Wishlist" href="#">
													<i className=" ti-heart "></i>
													<span>Añadir a la lista de deseos</span>
												</a>
												<a title="Compare" href="#">
													<i className="ti-bar-chart-alt"></i>
													<span>Añadir a comparar</span>
												</a>
											</div>
											<div className="product-action-2">
												<a title="Añadir al carrito" href="#">
													Añadir al carrito
												</a>
											</div>
										</div>
									</div>
									<div className="product-content">
										<h3>
											<a href="product-details.html">Awesome Pink Show</a>
										</h3>
										<div className="product-price">
											<span>$29.00</span>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 col-12">
								<div className="single-product">
									<div className="product-img">
										<a href="product-details.html">
											<img
												className="default-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<img
												className="hover-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
										</a>
										<div className="button-head">
											<div className="product-action">
												<a
													data-toggle="modal"
													data-target="#exampleModal"
													title="Quick View"
													href="#"
												>
													<i className=" ti-eye"></i>
													<span>Compra rápida</span>
												</a>
												<a title="Wishlist" href="#">
													<i className=" ti-heart "></i>
													<span>Añadir a la lista de deseos</span>
												</a>
												<a title="Compare" href="#">
													<i className="ti-bar-chart-alt"></i>
													<span>Añadir a comparar</span>
												</a>
											</div>
											<div className="product-action-2">
												<a title="Añadir al carrito" href="#">
													Añadir al carrito
												</a>
											</div>
										</div>
									</div>
									<div className="product-content">
										<h3>
											<a href="product-details.html">Awesome Bags Collection</a>
										</h3>
										<div className="product-price">
											<span>$29.00</span>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 col-12">
								<div className="single-product">
									<div className="product-img">
										<a href="product-details.html">
											<img
												className="default-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<img
												className="hover-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<span className="new">Nuevo</span>
										</a>
										<div className="button-head">
											<div className="product-action">
												<a
													data-toggle="modal"
													data-target="#exampleModal"
													title="Quick View"
													href="#"
												>
													<i className=" ti-eye"></i>
													<span>Compra rápida</span>
												</a>
												<a title="Wishlist" href="#">
													<i className=" ti-heart "></i>
													<span>Añadir a la lista de deseos</span>
												</a>
												<a title="Compare" href="#">
													<i className="ti-bar-chart-alt"></i>
													<span>Añadir a comparar</span>
												</a>
											</div>
											<div className="product-action-2">
												<a title="Añadir al carrito" href="#">
													Añadir al carrito
												</a>
											</div>
										</div>
									</div>
									<div className="product-content">
										<h3>
											<a href="product-details.html">Women Pant Collectons</a>
										</h3>
										<div className="product-price">
											<span>$29.00</span>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 col-12">
								<div className="single-product">
									<div className="product-img">
										<a href="product-details.html">
											<img
												className="default-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<img
												className="hover-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
										</a>
										<div className="button-head">
											<div className="product-action">
												<a
													data-toggle="modal"
													data-target="#exampleModal"
													title="Quick View"
													href="#"
												>
													<i className=" ti-eye"></i>
													<span>Compra rápida</span>
												</a>
												<a title="Wishlist" href="#">
													<i className=" ti-heart "></i>
													<span>Añadir a la lista de deseos</span>
												</a>
												<a title="Compare" href="#">
													<i className="ti-bar-chart-alt"></i>
													<span>Añadir a comparar</span>
												</a>
											</div>
											<div className="product-action-2">
												<a title="Añadir al carrito" href="#">
													Añadir al carrito
												</a>
											</div>
										</div>
									</div>
									<div className="product-content">
										<h3>
											<a href="product-details.html">Awesome Bags Collection</a>
										</h3>
										<div className="product-price">
											<span>$29.00</span>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 col-12">
								<div className="single-product">
									<div className="product-img">
										<a href="product-details.html">
											<img
												className="default-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<img
												className="hover-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<span className="price-dec">30% Off</span>
										</a>
										<div className="button-head">
											<div className="product-action">
												<a
													data-toggle="modal"
													data-target="#exampleModal"
													title="Quick View"
													href="#"
												>
													<i className=" ti-eye"></i>
													<span>Compra rápida</span>
												</a>
												<a title="Wishlist" href="#">
													<i className=" ti-heart "></i>
													<span>Añadir a la lista de deseos</span>
												</a>
												<a title="Compare" href="#">
													<i className="ti-bar-chart-alt"></i>
													<span>Añadir a comparar</span>
												</a>
											</div>
											<div className="product-action-2">
												<a title="Añadir al carrito" href="#">
													Añadir al carrito
												</a>
											</div>
										</div>
									</div>
									<div className="product-content">
										<h3>
											<a href="product-details.html">Awesome Cap For Women</a>
										</h3>
										<div className="product-price">
											<span>$29.00</span>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 col-12">
								<div className="single-product">
									<div className="product-img">
										<a href="product-details.html">
											<img
												className="default-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<img
												className="hover-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
										</a>
										<div className="button-head">
											<div className="product-action">
												<a
													data-toggle="modal"
													data-target="#exampleModal"
													title="Quick View"
													href="#"
												>
													<i className=" ti-eye"></i>
													<span>Compra rápida</span>
												</a>
												<a title="Wishlist" href="#">
													<i className=" ti-heart "></i>
													<span>Añadir a la lista de deseos</span>
												</a>
												<a title="Compare" href="#">
													<i className="ti-bar-chart-alt"></i>
													<span>Añadir a comparar</span>
												</a>
											</div>
											<div className="product-action-2">
												<a title="Añadir al carrito" href="#">
													Añadir al carrito
												</a>
											</div>
										</div>
									</div>
									<div className="product-content">
										<h3>
											<a href="product-details.html">Polo Dress For Women</a>
										</h3>
										<div className="product-price">
											<span>$29.00</span>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 col-12">
								<div className="single-product">
									<div className="product-img">
										<a href="product-details.html">
											<img
												className="default-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<img
												className="hover-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<span className="out-of-stock">Hot</span>
										</a>
										<div className="button-head">
											<div className="product-action">
												<a
													data-toggle="modal"
													data-target="#exampleModal"
													title="Quick View"
													href="#"
												>
													<i className=" ti-eye"></i>
													<span>Compra rápida</span>
												</a>
												<a title="Wishlist" href="#">
													<i className=" ti-heart "></i>
													<span>Añadir a la lista de deseos</span>
												</a>
												<a title="Compare" href="#">
													<i className="ti-bar-chart-alt"></i>
													<span>Añadir a comparar</span>
												</a>
											</div>
											<div className="product-action-2">
												<a title="Añadir al carrito" href="#">
													Añadir al carrito
												</a>
											</div>
										</div>
									</div>
									<div className="product-content">
										<h3>
											<a href="product-details.html">
												Black Sunglass For Women
											</a>
										</h3>
										<div className="product-price">
											<span className="old">$60.00</span>
											<span>$50.00</span>
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-4 col-md-6 col-12">
								<div className="single-product">
									<div className="product-img">
										<a href="product-details.html">
											<img
												className="default-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<img
												className="hover-img"
												src="https://via.placeholder.com/550x750"
												alt="#"
											/>
											<span className="new">New</span>
										</a>
										<div className="button-head">
											<div className="product-action">
												<a
													data-toggle="modal"
													data-target="#exampleModal"
													title="Quick View"
													href="#"
												>
													<i className=" ti-eye"></i>
													<span>Compra rápida</span>
												</a>
												<a title="Wishlist" href="#">
													<i className=" ti-heart "></i>
													<span>Añadir a la lista de deseos</span>
												</a>
												<a title="Compare" href="#">
													<i className="ti-bar-chart-alt"></i>
													<span>Añadir a comparar</span>
												</a>
											</div>
											<div className="product-action-2">
												<a title="Añadir al carrito" href="#">
													Añadir al carrito
												</a>
											</div>
										</div>
									</div>
									<div className="product-content">
										<h3>
											<a href="product-details.html">Women Pant Collectons</a>
										</h3>
										<div className="product-price">
											<span>$29.00</span>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<section className="shop-newsletter section">
			<div className="container">
				<div className="inner-top">
					<div className="row">
						<div className="col-lg-8 offset-lg-2 col-12">
							<div className="inner">
								<h4>Newsletter</h4>
								<p>
									{" "}
									Subscribe to our newsletter and get <span>10%</span> off your
									first purchase
								</p>
								<form
									action="mail/mail.php"
									method="get"
									target="_blank"
									className="newsletter-inner"
								>
									<input
										name="EMAIL"
										placeholder="Your email address"
										required=""
										type="email"
									/>
									<button className="btn">Subscribe</button>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

		<div className="modal fade" id="exampleModal" tabindex="-1" role="dialog">
			<div className="modal-dialog" role="document">
				<div className="modal-content">
					<div className="modal-header">
						<button
							type="button"
							className="close"
							data-dismiss="modal"
							aria-label="Close"
						>
							<span className="ti-close" aria-hidden="true"></span>
						</button>
					</div>
					<div className="modal-body">
						<div className="row no-gutters">
							<div className="col-lg-6 col-md-12 col-sm-12 col-xs-12">
								<div className="product-gallery">
									<div className="quickview-slider-active">
										<div className="single-slider">
											<img src="https://via.placeholder.com/569x528" alt="#" />
										</div>
										<div className="single-slider">
											<img src="https://via.placeholder.com/569x528" alt="#" />
										</div>
										<div className="single-slider">
											<img src="https://via.placeholder.com/569x528" alt="#" />
										</div>
										<div className="single-slider">
											<img src="https://via.placeholder.com/569x528" alt="#" />
										</div>
									</div>
								</div>
							</div>
							<div className="col-lg-6 col-md-12 col-sm-12 col-xs-12">
								<div className="quickview-content">
									<h2>Flared Shift Dress</h2>
									<div className="quickview-ratting-review">
										<div className="quickview-ratting-wrap">
											<div className="quickview-ratting">
												<i className="yellow fa fa-star"></i>
												<i className="yellow fa fa-star"></i>
												<i className="yellow fa fa-star"></i>
												<i className="yellow fa fa-star"></i>
												<i className="fa fa-star"></i>
											</div>
											<a href="#"> (1 customer review)</a>
										</div>
										<div className="quickview-stock">
											<span>
												<i className="fa fa-check-circle-o"></i> in stock
											</span>
										</div>
									</div>
									<h3>$29.00</h3>
									<div className="quickview-peragraph">
										<p>
											Lorem ipsum dolor sit amet, consectetur adipisicing elit.
											Mollitia iste laborum ad impedit pariatur esse optio
											tempora sint ullam autem deleniti nam in quos qui nemo
											ipsum numquam.
										</p>
									</div>
									<div className="size">
										<div className="row">
											<div className="col-lg-6 col-12">
												<h5 className="title">Size</h5>
												<select>
													<option selected="selected">s</option>
													<option>m</option>
													<option>l</option>
													<option>xl</option>
												</select>
											</div>
											<div className="col-lg-6 col-12">
												<h5 className="title">Color</h5>
												<select>
													<option selected="selected">orange</option>
													<option>purple</option>
													<option>black</option>
													<option>pink</option>
												</select>
											</div>
										</div>
									</div>
									<div className="quantity">
										<div className="input-group">
											<div className="button minus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													disabled="disabled"
													data-type="minus"
													data-field="quant[1]"
												>
													<i className="ti-minus"></i>
												</button>
											</div>
											<input
												type="text"
												name="quant[1]"
												className="input-number"
												data-min="1"
												data-max="1000"
												value="1"
											/>
											<div className="button plus">
												<button
													type="button"
													className="btn btn-primary btn-number"
													data-type="plus"
													data-field="quant[1]"
												>
													<i className="ti-plus"></i>
												</button>
											</div>
										</div>
									</div>
									<div className="add-to-cart">
										<a href="#" className="btn">
											Añadir al carrito
										</a>
										<a href="#" className="btn min">
											<i className="ti-heart"></i>
										</a>
										<a href="#" className="btn min">
											<i className="fa fa-compress"></i>
										</a>
									</div>
									<div className="default-social">
										<h4 className="share-now">Share:</h4>
										<ul>
											<li>
												<a className="facebook" href="#">
													<i className="fa fa-facebook"></i>
												</a>
											</li>
											<li>
												<a className="twitter" href="#">
													<i className="fa fa-twitter"></i>
												</a>
											</li>
											<li>
												<a className="youtube" href="#">
													<i className="fa fa-pinterest-p"></i>
												</a>
											</li>
											<li>
												<a className="dribbble" href="#">
													<i className="fa fa-google-plus"></i>
												</a>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
);

export default GridProducts;
