import React from "react";
import Page from "../../layout/main";
import Header from "../../components/header.js";
import Categories from "./components/Categories.js";
import GridProducts from "./components/GridProducts.js";
const Shop = () => (
	<Page>
		<Header />
		<GridProducts />
		<Categories />
	</Page>
);
export default Shop;
