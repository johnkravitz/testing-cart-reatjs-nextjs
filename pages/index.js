import Head from "next/head";
import Header from "../components/header.js";
const Home = () => (
	<div>
		<Head>
			<title>interStore</title>
			<link rel="icon" type="image/png" href="/assets/images/favicon.png" />
			<link
				href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"
				rel="stylesheet"
			/>

			<link rel="stylesheet" href="/assets/css/bootstrap.css" />
			<link rel="stylesheet" href="/assets/css/magnific-popup.min.css" />
			<link rel="stylesheet" href="/assets/css/font-awesome.css" />
			<link rel="stylesheet" href="/assets/css/jquery.fancybox.min.css" />
			<link rel="stylesheet" href="/assets/css/themify-icons.css" />
			<link rel="stylesheet" href="/assets/css/niceselect.css" />
			<link rel="stylesheet" href="/assets/css/animate.css" />
			<link rel="stylesheet" href="/assets/css/flex-slider.min.css" />
			<link rel="stylesheet" href="/assets/css/owl-carousel.css" />
			<link rel="stylesheet" href="/assets/css/slicknav.min.css" />

			<link rel="stylesheet" href="/assets/css/reset.css" />
			<link rel="stylesheet" href="/assets/style.css" />
			<link rel="stylesheet" href="/assets/css/responsive.css" />
		</Head>

		<main>
			<div className="preloader">
				<div className="preloader-inner">
					<div className="preloader-icon">
						<span></span>
						<span></span>
					</div>
				</div>
			</div>

			<Header />
			<div className="breadcrumbs">
				<div className="container">
					<div className="row">
						<div className="col-12">
							<div className="bread-inner">
								<ul className="bread-list">
									<li>
										<a href="index1.html">
											Home<i className="ti-arrow-right"></i>
										</a>
									</li>
									<li className="active">
										<a href="blog-single.html">Login</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<section className="shop login section">
				<div className="container">
					<div className="row">
						<div className="col-lg-6 offset-lg-3 col-12">
							<div className="login-form">
								<h2>Login</h2>
								<p>Please register in order to checkout more quickly</p>
								<form className="form" method="post" action="#">
									<div className="row">
										<div className="col-12">
											<div className="form-group">
												<label>
													Your Email<span>*</span>
												</label>
												<input
													type="email"
													name="email"
													placeholder=""
													required="required"
												/>
											</div>
										</div>
										<div className="col-12">
											<div className="form-group">
												<label>
													Your Password<span>*</span>
												</label>
												<input
													type="password"
													name="password"
													placeholder=""
													required="required"
												/>
											</div>
										</div>
										<div className="col-12">
											<div className="form-group login-btn">
												<button className="btn" type="submit">
													Login
												</button>
												<a href="register.html" className="btn">
													Register
												</a>
											</div>
											<div className="checkbox">
												<label className="checkbox-inline" for="2">
													<input name="news" id="2" type="checkbox" />
													Remember me
												</label>
											</div>
											<a href="#" className="lost-pass">
												Lost your password?
											</a>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</section>
		</main>

		<footer className="footer">
			<div className="footer-top section">
				<div className="container">
					<div className="row">
						<div className="col-lg-5 col-md-6 col-12">
							<div className="single-footer about">
								<div className="logo">
									<a href="index.html">
										<img src="assets/images/logo2.png" alt="#" />
									</a>
								</div>
								<p className="text">
									Praesent dapibus, neque id cursus ucibus, tortor neque egestas
									augue, magna eros eu erat. Aliquam erat volutpat. Nam dui mi,
									tincidunt quis, accumsan porttitor, facilisis luctus, metus.
								</p>
								<p className="call">
									Got Question? Call us 24/7
									<span>
										<a href="tel:123456789">+0123 456 789</a>
									</span>
								</p>
							</div>
						</div>
						<div className="col-lg-2 col-md-6 col-12">
							<div className="single-footer links">
								<h4>Information</h4>
								<ul>
									<li>
										<a href="#">About Us</a>
									</li>
									<li>
										<a href="#">Faq</a>
									</li>
									<li>
										<a href="#">Terms & Conditions</a>
									</li>
									<li>
										<a href="#">Contact Us</a>
									</li>
									<li>
										<a href="#">Help</a>
									</li>
								</ul>
							</div>
						</div>
						<div className="col-lg-2 col-md-6 col-12">
							<div className="single-footer links">
								<h4>Customer Service</h4>
								<ul>
									<li>
										<a href="#">Payment Methods</a>
									</li>
									<li>
										<a href="#">Money-back</a>
									</li>
									<li>
										<a href="#">Returns</a>
									</li>
									<li>
										<a href="#">Shipping</a>
									</li>
									<li>
										<a href="#">Privacy Policy</a>
									</li>
								</ul>
							</div>
						</div>
						<div className="col-lg-3 col-md-6 col-12">
							<div className="single-footer social">
								<h4>Get In Tuch</h4>
								<div className="contact">
									<ul>
										<li>NO. 342 - London Oxford Street.</li>
										<li>012 United Kingdom.</li>
										<li>info@eshop.com</li>
										<li>+032 3456 7890</li>
									</ul>
								</div>
								<ul>
									<li>
										<a href="#">
											<i className="ti-facebook"></i>
										</a>
									</li>
									<li>
										<a href="#">
											<i className="ti-twitter"></i>
										</a>
									</li>
									<li>
										<a href="#">
											<i className="ti-flickr"></i>
										</a>
									</li>
									<li>
										<a href="#">
											<i className="ti-instagram"></i>
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div className="copyright">
				<div className="container">
					<div className="inner">
						<div className="row">
							<div className="col-lg-6 col-12">
								<div className="left">
									<p>
										Copyright © 2020{" "}
										<a href="http://www.wpthemesgrid.com" target="_blank">
											Wpthemesgrid
										</a>{" "}
										- All Rights Reserved.
									</p>
								</div>
							</div>
							<div className="col-lg-6 col-12">
								<div className="right">
									<img src="assets/images/payments.png" alt="#" />
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</footer>

		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/js/jquery-migrate-3.0.0.js"></script>
		<script src="assets/js/jquery-ui.min.js"></script>
		<script src="assets/js/popper.min.js"></script>
		<script src="assets/js/bootstrap.min.js"></script>
		<script src="assets/js/colors.js"></script>
		<script src="assets/js/slicknav.min.js"></script>
		<script src="assets/js/owl-carousel.js"></script>
		<script src="assets/js/magnific-popup.js"></script>
		<script src="assets/js/waypoints.min.js"></script>
		<script src="assets/js/finalcountdown.min.js"></script>
		<script src="assets/js/nicesellect.js"></script>
		<script src="assets/js/flex-slider.js"></script>
		<script src="assets/js/scrollup.js"></script>
		<script src="assets/js/onepage-nav.min.js"></script>
		<script src="assets/js/easing.js"></script>
		<script src="assets/js/active.js"></script>
	</div>
);

export default Home;
