import Head from "next/head";
import Footer from "../components/footer";
const Default = ({ children, title = "Default" }) => (
	<div>
		<Head>
			<title>interStore</title>
			<link rel="icon" type="image/png" href="/assets/images/favicon.png" />
			<link
				href="https://fonts.googleapis.com/css?family=Poppins:200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i&display=swap"
				rel="stylesheet"
			/>

			<link rel="stylesheet" href="/assets/css/bootstrap.css" />
			<link rel="stylesheet" href="/assets/css/magnific-popup.min.css" />
			<link rel="stylesheet" href="/assets/css/font-awesome.css" />
			<link rel="stylesheet" href="/assets/css/jquery.fancybox.min.css" />
			<link rel="stylesheet" href="/assets/css/themify-icons.css" />
			<link rel="stylesheet" href="/assets/css/niceselect.css" />
			<link rel="stylesheet" href="/assets/css/animate.css" />
			<link rel="stylesheet" href="/assets/css/flex-slider.min.css" />
			<link rel="stylesheet" href="/assets/css/owl-carousel.css" />
			<link rel="stylesheet" href="/assets/css/slicknav.min.css" />

			<link rel="stylesheet" href="/assets/css/reset.css" />
			<link rel="stylesheet" href="/assets/style.css" />
			<link rel="stylesheet" href="/assets/css/responsive.css" />
		</Head>

		<main>
			{/* <div className="preloader">
				<div className="preloader-inner">
					<div className="preloader-icon">
						<span></span>
						<span></span>
					</div>
				</div>
			</div> */}

			{children}
		</main>
		<Footer />
		<script type="text/javascript" src="assets/js/jquery.min.js"></script>
		<script
			type="text/javascript"
			src="assets/js/jquery-migrate-3.0.0.js"
		></script>
		<script type="text/javascript" src="assets/js/jquery-ui.min.js"></script>
		<script type="text/javascript" src="assets/js/popper.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.min.js"></script>
		<script type="text/javascript" src="assets/js/colors.js"></script>
		<script type="text/javascript" src="assets/js/slicknav.min.js"></script>
		<script type="text/javascript" src="assets/js/owl-carousel.js"></script>
		<script type="text/javascript" src="assets/js/magnific-popup.js"></script>
		<script type="text/javascript" src="assets/js/waypoints.min.js"></script>
		<script
			type="text/javascript"
			src="assets/js/finalcountdown.min.js"
		></script>
		<script type="text/javascript" src="assets/js/nicesellect.js"></script>
		<script type="text/javascript" src="assets/js/flex-slider.js"></script>
		<script type="text/javascript" src="assets/js/scrollup.js"></script>
		<script type="text/javascript" src="assets/js/onepage-nav.min.js"></script>
		<script type="text/javascript" src="assets/js/easing.js"></script>
		<script type="text/javascript" src="assets/js/active.js"></script>
	</div>
);

export default Default;
