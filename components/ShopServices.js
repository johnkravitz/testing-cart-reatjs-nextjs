import React from "react";

const ShopServices = () => (
	<section class="shop-services section home">
		<div class="container">
			<div class="row">
				<div class="col-lg-3 col-md-6 col-12">
					<div class="single-service">
						<i class="ti-rocket"></i>
						<h4>Envío gratis</h4>
						<p>Pedidos de más de $100</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-12">
					<div class="single-service">
						<i class="ti-reload"></i>
						<h4>Devolución gratuita</h4>
						<p>Dentro de 30 días regresa</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-12">
					<div class="single-service">
						<i class="ti-lock"></i>
						<h4>Pago seguro</h4>
						<p>100% Pago Seguro</p>
					</div>
				</div>
				<div class="col-lg-3 col-md-6 col-12">
					<div class="single-service">
						<i class="ti-tag"></i>
						<h4>Mejor Precio</h4>
						<p>Precio Garantizado</p>
					</div>
				</div>
			</div>
		</div>
	</section>
);

export default ShopServices;
